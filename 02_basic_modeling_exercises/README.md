# Basic Modeling [2020]

## Models Overview

### Cup

![Cup](/images/readme/basic_modeling/cup.png "Cup")

### Apple

![Apple](/images/readme/basic_modeling/apple.png "Apple")

### Shampoo Bottle

![Shampoo Bottle](/images/readme/basic_modeling/shampoo_bottle.png "Shampoo Bottle")

### Headphones

![Headphones](/images/readme/basic_modeling/headphones.png "Headphones")

### Robot

![Robot](/images/readme/basic_modeling/robot.png "Robot")

