# Curves Modeling [2020]

## Models Overview

### Headphones

![Headphones](/images/readme/curves_modeling/headphones.png "Headphones")

### Text

![Text](/images/readme/curves_modeling/text.png "Text")

### 3D Logo

![3D Logo](/images/readme/curves_modeling/3d_logo.png "3D Logo")

