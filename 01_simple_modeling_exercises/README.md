# Simple Modeling [2020]

## Models Overview

### Bed

![Bed](/images/readme/simple_modeling/bed.png "Bed")

### Chair

![Chair](/images/readme/simple_modeling/chair.png "Chair")

### Cup

![Cup](/images/readme/simple_modeling/cup.png "Cup")

### Desk

![Desk](/images/readme/simple_modeling/desk.png "Desk")

### Lamp

![Lamp](/images/readme/simple_modeling/lamp.png "Lamp")

### Table

![Table](/images/readme/simple_modeling/table.png "Table")
